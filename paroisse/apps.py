from django.apps import AppConfig


class ParoisseConfig(AppConfig):
    name = 'paroisse'
