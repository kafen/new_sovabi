# coding: utf-8

from django.conf.urls import url

from paroisse import views

urlpatterns = [
    url(r'^$', views.homepage,
        name="homepage"),
    url(r'^celebrations/liste/$', views.liste_celebrations,
        name="celebrations_liste"),
]