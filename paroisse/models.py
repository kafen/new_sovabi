from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from sovabi.models import SovabiModel


class Lieu(models.Model, SovabiModel):   
    nom = models.CharField(max_length=150, blank=False)

    def __str__(self):
        return u"{0}".format(self.nom)

    class Meta:
        verbose_name_plural = "Lieux"


class Personne(models.Model, SovabiModel):   
    class Meta:
        abstract = True


class Anonyme(Personne):   
    prenom = models.CharField(max_length=150, blank=False)
    nom = models.CharField(max_length=150, blank=False)

    def __str__(self):
        return u"{0} {1}".format(self.prenom, self.nom.toupper())


class Paroissien(Personne):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)

    prenom = models.CharField(max_length=150, blank=False)
    nom = models.CharField(max_length=150, blank=False)
    
    actif = models.BooleanField(default=True)
    dead = models.BooleanField(default=False)

    diacre = models.BooleanField(default=False)
    pretre = models.BooleanField(default=False)

    sexe = models.CharField(max_length=1, 
       choices=(
                ('M', 'Male'),  
                ('F', 'Female')
       )
    )
    naissance = models.DateField(null=True)
    bapteme = models.DateField(null=True)
    communion = models.DateField(null=True)
    profession_foi = models.DateField(null=True)
    confirmation = models.DateField(null=True)
    rgpd_consent = models.DateField(null=True)

    langue_maternelle = models.CharField(max_length=20)

    telephone = models.CharField(max_length=50)
    adresse = models.CharField(max_length=150)

    pere = models.OneToOneField('Paroissien', null=True, blank=True, on_delete=models.SET_NULL, related_name="+")
    mere = models.OneToOneField('Paroissien', null=True, blank=True, on_delete=models.SET_NULL, related_name="+")
    conjoint = models.OneToOneField('Paroissien', null=True, blank=True, on_delete=models.SET_NULL, related_name="+")

    confession = models.CharField(max_length=15)
    souhaits = models.IntegerField()
    commentaire = models.CharField(max_length=350)

    def __str__(self):
        return u"{0} {1}".format(self.prenom, self.nom.toupper())


class TypeCelebration(models.Model, SovabiModel):   
    nom = models.CharField(max_length=150, blank=False)

    def __str__(self):
        return u"{0}".format(self.nom)

    class Meta:
        verbose_name = "Type de célébration"
        verbose_name_plural = "Types de célébration"


class Celebration(models.Model, SovabiModel):
    celebration_date = models.DateField(
        _('date of the event')
    )
    celebration_time = models.TimeField(
        _('hour of the event'),
    )
    place = models.ForeignKey(
        'Lieu',
        on_delete=models.SET_DEFAULT, default=0, null=False,
        verbose_name=_('place'),related_name="messes"
    )
    messe = models.OneToOneField(
        'Messe',
        on_delete=models.SET_DEFAULT,
        default=None, blank=True, null=True,
        related_name="celebration"
    )
    baptemes = models.ForeignKey(
        'Bapteme',
        on_delete=models.SET_DEFAULT,
        default=None, blank=True, null=True,
        related_name="celebration"
    )
    mariages = models.ForeignKey(
        'Mariage',
        on_delete=models.SET_DEFAULT,
        default=None, blank=True, null=True,
        related_name="celebration"
    )

    def __str__(self):
        return u"{0} {1} at {2}".format(self.celebration_date, self.celebration_time, self.place)


class Messe(models.Model, SovabiModel):
    type_celebration = models.ForeignKey(
        'TypeCelebration',
        on_delete=models.SET_DEFAULT, default=0, null=False, related_name="messes"
    )

    # def __str__(self):
    #    return u""


class Bapteme(models.Model, SovabiModel):
    pass
    # baptise = models.ForeignKey(
    #     'TypeCelebration',
    #     on_delete=models.SET_DEFAULT, default=0, null=False, related_name="messes"
    # )

    # def __str__(self):
    #    return u""


class Mariage(models.Model, SovabiModel):
    pass
    # baptise = models.ForeignKey(
    #     'TypeCelebration',
    #     on_delete=models.SET_DEFAULT, default=0, null=False, related_name="messes"
    # )

    # def __str__(self):
    #    return u""