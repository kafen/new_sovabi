from django.contrib import admin

from paroisse.models import (
    Lieu,
    TypeCelebration,
    Celebration,
    Bapteme,
    Mariage,
    Messe
)

admin.site.register(Lieu)
admin.site.register(Celebration)
admin.site.register(TypeCelebration)
admin.site.register(Messe)
admin.site.register(Bapteme)
admin.site.register(Mariage)