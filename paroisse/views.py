from django.shortcuts import render

from paroisse.models import Celebration

def liste_celebrations(request):
    celebrations = Celebration.objects.all()

    return render(request, 'celebrations/liste.html', {
        "celebrations":celebrations,
    })


def homepage(request):
    return render(request, 'base/homepage.html', {})